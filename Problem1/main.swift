//  Problem1.swift
//  assign-2-2016
//
//  Created by Frank  Niscak on 2016-06-15.
//  Copyright (c) 2015 NIC. All rights reserved.
//

// Project:      assign-2-2016
// File:         main.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Write an application that prompts for and reads integer values for speed
// and distance traveled, then prints the time required for the trip as a
// floating point result.
//
// Inputs:   speed and distance as integer numbers
// Outputs:  time traveled as a floating point number


import Foundation

let calculator: TimeCalculator = TimeCalculator(distance: 150, speed: 50)

calculator.getUserInput()

calculator.calculateTime()

let message:String = calculator.description()
print(message)

