//
//  TimeCalculator.swift
//  hi-assign2
//
//  Created by Student on 2016-10-17.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

public class TimeCalculator {
  
  var distance: Int
  var speed: Int
  var time: Double
  
  public init(distance: Int, speed: Int) {
    self.distance = distance
    self.speed = speed
    self.time = Double(distance/speed)
  }
  
  public func calculateTime() {
    time = Double(distance / speed)
  }
  
  public func getUserInput() {
    print("Inform the distance (in km):")
    self.distance = IOLibrary.getIntFromConsole()
    print("Inform the speed (in km/h):")
    self.speed = IOLibrary.getIntFromConsole()
  }
  
  public func description() -> String {
    calculateTime()
    return "The time required fot the trip is: \(time) hours."
  }
  
}
