//  Problem2.swift
//  assign-2-2016
//
//  Created by Frank  Niscak on 2016-06-15.
//  Copyright (c) 2015 NIC. All rights reserved.
//
// Project:      assign-2-2016
// File:         main.swift
// Author:       Ismael F. Hepp
// Date:         Oct 18, 2016
// Course:       IMG204
//
// Problem Statement:
// Create a version of the previous project that reverses the computation.
// That is, read a value representing a number of seconds, then print the
// equivalent amount of time as a combination of hours, minutes, and seconds.
// (For example, 9999 seconds is equivalent to 2 hours, 46 minutes,
// and 39 seconds.)
//
// Inputs:   total seconds as an integer number
// Outputs:  number of hour, minutes, and seconds

import Foundation

let timeConverter: TimeConverter = TimeConverter(totalSeconds: 3661)

timeConverter.getUserInput()

timeConverter.convertTime()

timeConverter.displayTimeInfo()

print(timeConverter.description)


