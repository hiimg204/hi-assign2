//
//  TimeConverter.swift
//  hi-assign2
//
//  Created by Student on 2016-10-17.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation

public class TimeConverter : CustomStringConvertible {
  
  var totalSeconds: Int
  var hours: Double = 0
  var minutes: Double = 0
  var seconds: Int = 0
  
  public var description: String {
    convertTime()
    return "Hours: \(hours)\tMinutes: \(minutes)\tSeconds: \(seconds)"
  }
  
  public init(totalSeconds: Int) {
    self.totalSeconds = totalSeconds
  }
  
  public func getUserInput() {
    print("Inform the time (in seconds):")
    self.totalSeconds = IOLibrary.getIntFromConsole()
  }
  
  public func convertTime() {
    seconds = (totalSeconds)
    hours = Double(seconds/3600)
    seconds = seconds % 3600
    minutes = Double(seconds/60)
    seconds = seconds % 60
  }
  
  public func displayTimeInfo() {
    print(description)
  }
      
}
